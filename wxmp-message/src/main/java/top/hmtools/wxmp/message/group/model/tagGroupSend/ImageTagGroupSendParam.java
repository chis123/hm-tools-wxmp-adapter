package top.hmtools.wxmp.message.group.model.tagGroupSend;

/**
 * 图片（注意此处media_id需通过素材管理->新增素材来得到）：		
 * @author HyboWork
 *
 */
public class ImageTagGroupSendParam extends BaseTagGroupSendParam {

	private MediaId image;

	public MediaId getVoice() {
		return image;
	}

	public void setVoice(MediaId voice) {
		this.image = voice;
	}

	@Override
	public String toString() {
		return "VoiceTagGroupSendParam [image=" + image + ", filter=" + filter + ", msgtype=" + msgtype + "]";
	}

}
