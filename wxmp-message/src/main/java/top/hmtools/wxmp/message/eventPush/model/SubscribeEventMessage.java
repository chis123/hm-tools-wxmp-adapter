package top.hmtools.wxmp.message.eventPush.model;

import com.thoughtworks.xstream.XStream;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseEventMessage;
import top.hmtools.wxmp.core.model.message.enums.Event;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * 关注/取消关注事件
 * {@code
 * <xml>
	<ToUserName><![CDATA[toUser]]></ToUserName>
	<FromUserName><![CDATA[FromUser]]></FromUserName>
	<CreateTime>123456789</CreateTime>
	<MsgType><![CDATA[event]]></MsgType>
	<Event><![CDATA[subscribe]]></Event>
</xml>
 * }
 * @author Hybomyth
 *
 */
@WxmpMessage(msgType=MsgType.event,event=Event.subscribe)
public class SubscribeEventMessage extends BaseEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3401546644926460381L;

	@Override
	public void configXStream(XStream xStream) {
		
	}

	@Override
	public String toString() {
		return "SubscribeEventMessage [event=" + event + ", eventKey=" + eventKey + ", toUserName=" + toUserName
				+ ", fromUserName=" + fromUserName + ", createTime=" + createTime + ", msgType=" + msgType + ", msgId="
				+ msgId + "]";
	}

	
	

}
