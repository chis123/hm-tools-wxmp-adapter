package top.hmtools.wxmp.message.group.model.openIdGroupSend;

import java.util.List;

import top.hmtools.wxmp.message.group.model.tagGroupSend.TagMsgType;

/**
 * Auto-generated: 2019-08-27 16:41:27
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class BaseOpenIdGroupSendParam {

	protected TagMsgType msgtype;
	
	protected List<String> touser;

	public TagMsgType getMsgtype() {
		return msgtype;
	}

	public void setMsgtype(TagMsgType msgtype) {
		this.msgtype = msgtype;
	}

	public List<String> getTouser() {
		return touser;
	}

	public void setTouser(List<String> touser) {
		this.touser = touser;
	}

	@Override
	public String toString() {
		return "BaseOpenIdGroupSendParam [msgtype=" + msgtype + ", touser=" + touser + "]";
	}



}