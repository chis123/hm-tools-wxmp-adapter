package top.hmtools.wxmp.message.template.model;

public class TemplateIdParam {

	private String template_id_short;

	public String getTemplate_id_short() {
		return template_id_short;
	}

	public void setTemplate_id_short(String template_id_short) {
		this.template_id_short = template_id_short;
	}
	
	
}
