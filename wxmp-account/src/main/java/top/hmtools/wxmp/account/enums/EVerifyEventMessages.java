package top.hmtools.wxmp.account.enums;

import top.hmtools.wxmp.account.models.eventMessage.AnnualRenewMessage;
import top.hmtools.wxmp.account.models.eventMessage.NamingVerifyFailMessage;
import top.hmtools.wxmp.account.models.eventMessage.NamingVerifySuccessMessage;
import top.hmtools.wxmp.account.models.eventMessage.QualificationVerifyFailMessage;
import top.hmtools.wxmp.account.models.eventMessage.QualificationVerifySuccessMessage;
import top.hmtools.wxmp.account.models.eventMessage.VerifyExpiredMessage;

/**
 * 自定义菜单事件实体类枚举集合
 * @author HyboWork
 *
 */
public enum EVerifyEventMessages {
	
	qualification_verify_success("资质认证成功（此时立即获得接口权限）",QualificationVerifySuccessMessage.class),
	qualification_verify_fail("资质认证失败",QualificationVerifyFailMessage.class),
	naming_verify_success("名称认证成功（即命名成功）",NamingVerifySuccessMessage.class),
	naming_verify_fail("名称认证失败（这时虽然客户端不打勾，但仍有接口权限）",NamingVerifyFailMessage.class),
	annual_renew("年审通知",AnnualRenewMessage.class),
	verify_expired("认证过期失效通知",VerifyExpiredMessage.class)
	;

	private String eventName;
	
	private Class<?> className;
	
	private String remark;
	
	private EVerifyEventMessages(String eName,Class<?> clazz) {
		this.eventName = eName;
		this.className = clazz;
	}

	public String getEventName() {
		return eventName;
	}

	public Class<?> getClassName() {
		return className;
	}

	public String getRemark() {
		return remark;
	}

	
	
}
