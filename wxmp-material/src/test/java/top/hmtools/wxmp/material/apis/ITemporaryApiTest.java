package top.hmtools.wxmp.material.apis;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

import org.apache.commons.io.IOUtils;
import org.junit.Test;

import top.hmtools.wxmp.material.BaseTest;
import top.hmtools.wxmp.material.enums.MediaType;
import top.hmtools.wxmp.material.model.MediaBean;
import top.hmtools.wxmp.material.model.MediaParam;
import top.hmtools.wxmp.material.model.UploadParam;
import top.hmtools.wxmp.material.model.VideoResult;

public class ITemporaryApiTest extends BaseTest {
	
	
	ITemporaryApi temporaryApi;
	

	@Test
	public void testUploadMedia() {
		File file = new File("E:\\tmp\\aaaa.png");
		if(!file.exists()){
			System.out.println(file+"文件不存在");
			return;
		}
		
		UploadParam uploadParam = new UploadParam();
		uploadParam.setMedia(file);
		uploadParam.setType(MediaType.image);
		MediaBean mediaBean = this.temporaryApi.uploadMedia(uploadParam);
		this.printFormatedJson("素材管理--新增临时素材--图片", mediaBean);
	}

	@Test
	public void testGetImage() throws IOException {
		MediaParam mediaParam = new MediaParam();
		mediaParam.setMedia_id("yZnVC5-PEbBZuUhXLzQJJe_kDYLv4XJONIrSm6ejX9Csgwx5e2xoBTnAfZzEPCON");
		InputStream image = this.temporaryApi.getImage(mediaParam);
		this.printFormatedJson("素材管理--获取临时素材--图片", IOUtils.toByteArray(image));
	}

	@Test
	public void testGetVideo() {
		File file = new File("E:\\tmp\\bbb.mp4");
		if(!file.exists()){
			System.out.println(file+"文件不存在");
			return;
		}
		
		UploadParam uploadParam = new UploadParam();
		uploadParam.setMedia(file);
		uploadParam.setType(MediaType.video);
		MediaBean mediaBean = this.temporaryApi.uploadMedia(uploadParam);
		System.out.println(mediaBean);
		this.printFormatedJson("素材管理--新增临时素材--视频", mediaBean);
		
		MediaParam mediaParam = new MediaParam();
		mediaParam.setMedia_id(mediaBean.getMedia_id());
		
		VideoResult video = this.temporaryApi.getVideo(mediaParam);
		this.printFormatedJson("素材管理--获取临时素材--视频", video);
	}

	@Test
	public void testGetVoice() {
		//TODO 需要结合js-sdk前端上传音频接口
	}

	@Override
	public void initSub() {
		this.temporaryApi = this.wxmpSession.getMapper(ITemporaryApi.class);
	}

}
