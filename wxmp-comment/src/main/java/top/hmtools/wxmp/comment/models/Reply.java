package top.hmtools.wxmp.comment.models;

/**
 * Auto-generated: 2019-08-21 22:16:6
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Reply {

	/**
	 * 作者回复内容
	 */
	private String content;

	/**
	 * 作者回复时间
	 */
	private String create_time;

	public void setContent(String content) {
		this.content = content;
	}

	public String getContent() {
		return content;
	}

	public void setCreate_time(String create_time) {
		this.create_time = create_time;
	}

	public String getCreate_time() {
		return create_time;
	}

	@Override
	public String toString() {
		return "Reply [content=" + content + ", create_time=" + create_time + "]";
	}

}