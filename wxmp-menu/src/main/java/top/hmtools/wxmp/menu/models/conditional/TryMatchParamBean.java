package top.hmtools.wxmp.menu.models.conditional;

/**
 * 测试个性化菜单匹配结果 接口用 请求参数描述实体类
 * @author Hybomyth
 *
 */
public class TryMatchParamBean {

	/**
	 * user_id可以是粉丝的OpenID，也可以是粉丝的微信号。
	 */
	private String user_id;

	/**
	 * user_id可以是粉丝的OpenID，也可以是粉丝的微信号。
	 * @return
	 */
	public String getUser_id() {
		return user_id;
	}

	/**
	 * user_id可以是粉丝的OpenID，也可以是粉丝的微信号。
	 * @param user_id
	 */
	public void setUser_id(String user_id) {
		this.user_id = user_id;
	}

	@Override
	public String toString() {
		return "TryMatchParamBean [user_id=" + user_id + "]";
	}
	
	
}
