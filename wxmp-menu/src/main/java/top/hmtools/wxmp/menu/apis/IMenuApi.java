package top.hmtools.wxmp.menu.apis;

import top.hmtools.wxmp.core.annotation.WxmpApi;
import top.hmtools.wxmp.core.annotation.WxmpMapper;
import top.hmtools.wxmp.core.enums.HttpMethods;
import top.hmtools.wxmp.core.model.ErrcodeBean;
import top.hmtools.wxmp.menu.models.simple.CurrentSelfMenuInfoBean;
import top.hmtools.wxmp.menu.models.simple.MenuBean;
import top.hmtools.wxmp.menu.models.simple.MenuWapperBean;

@WxmpMapper
public interface IMenuApi {
	
	/**
	 * 创建自定义菜单目录
	 * @param menuBean
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.POST,uri = "/cgi-bin/menu/create")
	public ErrcodeBean createMenu(MenuBean menuBean);

	/**
	 * 查询自定义菜单目录
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.GET, uri = "/cgi-bin/menu/get")
	public MenuWapperBean getMenu();
	
	/**
	 * 删除全部自定义菜单信息
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.GET, uri = "/cgi-bin/menu/delete")
	public ErrcodeBean deleteAllMenu();
	
	/**
	 * 获取自定义菜单配置接口
	 * @return
	 */
	@WxmpApi(httpMethods=HttpMethods.GET,uri = "/cgi-bin/get_current_selfmenu_info")
	public CurrentSelfMenuInfoBean getCurrentSelfMenuInfo();
}
